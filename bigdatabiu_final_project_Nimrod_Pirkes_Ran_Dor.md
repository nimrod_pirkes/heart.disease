---
title: "Big Data BIU - Project Heart Disease"
author: "Nimrod Pirkes*, Ran Dor*"
output:
  html_document:
    keep_md: yes
  pdf_document: default
---







# Introduction

You can find our repository in the link: "git clone git@bitbucket.org:nimrod_pirkes/heart.disease.git"

#### Research question  
Our research question is if we can declare whether or not a person will have a heart disease according to his data parameters.  

```r
library(imager)
myimg <- load.image("image1.png")
plot(myimg)
```

<img src="figure/manual-unnamed-chunk-2-1.png" style="display: block; margin: auto;" />
#### Cases
The structure of the database is built from the following columns and the rows represent different people.  

The columns are the different parameters which we will use in order to answer our research question.   
age - age in years  
sex - (1 = male; 0 = female)  
cp - chest pain type  
trestbps - resting blood pressure (in mm Hg on admission to the hospital)  
chol - serum cholestoral in mg/dl  
fbs - (fasting blood sugar > 120 mg/dl) (1 = true; 0 = false)  
restecg - resting electrocardiographic results  
thalach - maximum heart rate achieved  
exang - exercise induced angina (1 = yes; 0 = no)  
oldpeak - ST depression induced by exercise relative to rest  
slope - the slope of the peak exercise ST segment  
ca - number of major vessels (0-3) colored by flourosopy  
thal - 2 = normal(3.0); 1 = fixed defect(6.0); 3 = reversable defect(7.0)  
Heart Disease - 1-4 or 0 if person is healthy (0) or the disease's strength 


#### Data collection
This database contains 76 attributes, but all published experiments refer to using a subset of 14 of them. In particular, the Cleveland database is the only one that has been used by ML researchers to 
this date. The "goal" field refers to the presence of heart disease in the patient. It is integer valued from 0 (no presence) to 4. Experiments with the Cleveland database have concentrated on simply attempting to distinguish presence (values 1,2,3,4) from absence (value 0). 


#### Type of study
We received our data from blood checks and a survey from the patients of this data, so the type of study is observational.


#### Data Source 
If you collected the data, state self-collected. If not, provide a citation/link.  
Our data was not self-collected and therefore the link is :  
"https://www.kaggle.com/imabhilash/heart-disease-uci"


# Data Overview

#### Loading Libraries

```r
library(ggplot2)
library(Hmisc)
library(corrplot)
library(dplyr)
library(tidyr)
library(caret)
library(rpart)
library(rpart.plot)
library(doParallel)
library(e1071)
library(xgboost)
```

#### Loading dataset

```r
dataset <- read.csv("heart.csv",header = TRUE,sep=",")
dataset <- dataset[,-1] # delete "X"(index) column
#dataset$Heart.Disease <- as.factor(dataset$Heart.Disease)
str(dataset)  
```

```
## 'data.frame':	303 obs. of  14 variables:
##  $ age          : int  63 37 41 56 57 57 56 44 52 57 ...
##  $ sex          : int  1 1 0 1 0 1 0 1 1 1 ...
##  $ cp           : int  3 2 1 1 0 0 1 1 2 2 ...
##  $ trestbps     : int  145 130 130 120 120 140 140 120 172 150 ...
##  $ chol         : int  233 250 204 236 354 192 294 263 199 168 ...
##  $ fbs          : int  1 0 0 0 0 0 0 0 1 0 ...
##  $ restecg      : int  0 1 0 1 1 1 0 1 1 1 ...
##  $ thalach      : int  150 187 172 178 163 148 153 173 162 174 ...
##  $ exang        : int  0 0 0 0 1 0 0 0 0 0 ...
##  $ oldpeak      : num  2.3 3.5 1.4 0.8 0.6 0.4 1.3 0 0.5 1.6 ...
##  $ slope        : int  0 0 2 2 2 1 1 2 2 2 ...
##  $ ca           : int  0 0 0 0 0 0 0 0 0 0 ...
##  $ thal         : int  1 2 2 2 2 1 2 3 3 2 ...
##  $ Heart.Disease: int  1 1 4 1 0 4 0 1 4 4 ...
```
#### data overview


```r
head(dataset)
```

```
##   age sex cp trestbps chol fbs restecg thalach exang oldpeak slope ca thal
## 1  63   1  3      145  233   1       0     150     0     2.3     0  0    1
## 2  37   1  2      130  250   0       1     187     0     3.5     0  0    2
## 3  41   0  1      130  204   0       0     172     0     1.4     2  0    2
## 4  56   1  1      120  236   0       1     178     0     0.8     2  0    2
## 5  57   0  0      120  354   0       1     163     1     0.6     2  0    2
## 6  57   1  0      140  192   0       1     148     0     0.4     1  0    1
##   Heart.Disease
## 1             1
## 2             1
## 3             4
## 4             1
## 5             0
## 6             4
```

# EDA - Exploratory data analysis

In statistics, exploratory data analysis (EDA) is an approach to analyzing data sets to summarize their main characteristics, often with visual methods. A statistical model can be used or not, but primarily EDA is for seeing what the data can tell us beyond the formal modeling or hypothesis testing task. Exploratory data analysis was promoted by John Tukey to encourage statisticians to explore the data, and possibly formulate hypotheses that could lead to new data collection and experiments.

#### The distribution of the Disease levels

Y axis represent the number of patients in each level.


```r
ggplot(dataset, aes(x = as.factor(Heart.Disease))) + geom_bar() + ggtitle("Disease level vs count (Levels 0 (healthy)  to 4)") + labs(x = "Heart Disease Level", y = "Count") + theme(plot.title = element_text(hjust = 0.5))
```

<img src="figure/manual-unnamed-chunk-6-1.png" style="display: block; margin: auto;" />

#### Gender vs Heart disease  

This plot indicates the heart disease's strength according to the sex of the patient. Our data shows us that there is a higher precent of a male to have a disease rather than a female. Also there are more males that have a strong heart disease (level 4).  
In addition we see that there are more males that participated in this data than females.  


```r
ggplot(dataset, aes(x = as.factor(sex))) + geom_bar() + ggtitle("Gender count (0 - Female, 1 - Male)")+ theme(plot.title = element_text(hjust = 0.5))
mosaicplot(dataset$sex ~ as.factor(dataset$Heart.Disease), main="Disease by Gender", shade=FALSE,color=blues9,
           xlab="Gender- 0=female, 1=male", ylab="Heart disease") 
```

<img src="figure/manual-unnamed-chunk-7-1.png" style="display: block; margin: auto;" /><img src="figure/manual-unnamed-chunk-7-2.png" style="display: block; margin: auto;" />

#### Chances of getting heart disease depending on age
This plot shows us the heart disease according to the age of the patients. From the graph below we can say that most heart diseases accure between the ages of 40 to 70 years old. Most of the strongest heart disease (level 4) accure between the ages of 30 to 70.  

```r
ggplot(data = dataset, mapping = aes(x = age)) + 
  geom_freqpoly(mapping = aes(colour = as.factor(Heart.Disease)), binwidth = 5) + ggtitle("Heart Disease vs Age") + labs(colour = "Heart Disease Level", x = "Age", y = "Count") + theme(plot.title = element_text(hjust = 0.5))
```

<img src="figure/manual-unnamed-chunk-8-1.png" style="display: block; margin: auto;" />

#### Chances of getting heart disease depending on cholesterol
This plot indicates the heart disease according to the cholesterol in the patients body. We can see that when the human body is in the range of cholesterol, 275 to 375 there is a almost zero precent chance that you will be sick with a heart disease. and less than 275, you will almost definetly have a heart disease.   


```r
ggplot(data = dataset, mapping = aes(x = chol)) + 
  geom_freqpoly(mapping = aes(colour = as.factor(Heart.Disease)), binwidth = 5) + ggtitle("Hreat Disease vs Cholesterol") + labs(x = "Cholesterol", y = "Count", colour = "Heart Disease Level") + theme(plot.title = element_text(hjust = 0.5))
```

<img src="figure/manual-unnamed-chunk-9-1.png" style="display: block; margin: auto;" />
 
#### box plot - Cholesterol vs Heart.Disease vs sex  
This plot indicates the heart disease according to the cholesterol in the patients body by sex. This plot is very similar to the previouse graph only that in this box plot we also have the sex parameter. From the graph we notice that the female sex has a lower cholesterol than the male for healthy patients.  
  

```r
ggplot(dataset, aes(y = chol, x = as.factor(Heart.Disease), fill = as.factor(dataset$sex))) + geom_boxplot() + ggtitle("Hreat Disease vs Cholesterol vs Sex") + labs(x = "Heart Disease Level", y = "Cholesterol", fill = "Sex") + theme(plot.title = element_text(hjust = 0.5))
```

<img src="figure/manual-unnamed-chunk-10-1.png" style="display: block; margin: auto;" />

#### Checking Correlation  
This plot presents the correlation between all the parameters and show the level of the correlation between them. We can recognize that heart disease and chol levels are negatively correlated. Moreover, thalach and slope are positively correlated.

```r
#install.packages("corrplot")
test <- cor(dataset);
head(round(test,2))
```

```
##            age   sex    cp trestbps  chol  fbs restecg thalach exang
## age       1.00 -0.10 -0.07     0.28  0.21 0.12   -0.12   -0.40  0.10
## sex      -0.10  1.00 -0.05    -0.06 -0.20 0.05   -0.06   -0.04  0.14
## cp       -0.07 -0.05  1.00     0.05 -0.08 0.09    0.04    0.30 -0.39
## trestbps  0.28 -0.06  0.05     1.00  0.12 0.18   -0.11   -0.05  0.07
## chol      0.21 -0.20 -0.08     0.12  1.00 0.01   -0.15   -0.01  0.07
## fbs       0.12  0.05  0.09     0.18  0.01 1.00   -0.08   -0.01  0.03
##          oldpeak slope    ca  thal Heart.Disease
## age         0.21 -0.17  0.28  0.07         -0.29
## sex         0.10 -0.03  0.12  0.21          0.03
## cp         -0.15  0.12 -0.18 -0.16          0.13
## trestbps    0.19 -0.12  0.10  0.06         -0.13
## chol        0.05  0.00  0.07  0.10         -0.61
## fbs         0.01 -0.06  0.14 -0.03         -0.02
```

```r
corrplot(test, method="circle")
```

<img src="figure/manual-unnamed-chunk-11-1.png" style="display: block; margin: auto;" />

#### Building Machine learning Models  

First, we split the data to train and test sets.Then, we check that we have the same features distribution in both of the sets. We can recognize a very close distributions, means that our split is good for the following process. 

```r
dataset2 <- dataset
# change heart disease column to binary level 0/1 
dataset2$Heart.Disease <- replace(dataset2$Heart.Disease, dataset2$Heart.Disease > 0, 1)

# Split to training and test set 0.7/0.3
set.seed(42)
index <- createDataPartition(dataset$Heart.Disease, p = 0.7, list = FALSE)
train_data <- dataset[index, ]
test_data  <- dataset[-index, ]

# Checking the splitting to train and test
rbind(data.frame(group = "train", train_data),
      data.frame(group = "test", test_data)) %>%
      gather(key = f, value = v ,age:Heart.Disease) %>%
  ggplot(aes(x = v, color = group, fill = group)) +
    geom_density(alpha = 0.3) +
    facet_wrap( ~ f, scales = "free", ncol = 3)
```

<img src="figure/manual-unnamed-chunk-12-1.png" style="display: block; margin: auto;" />
  
  
Applying correlation test on the train data without Heart Disease column and filter features with more than 0.7(or -0.7) value of correlation. We see that we have no features with 0.7 absolute correlation value.  

#### Correlation Matrix

```r
# calculate correlation matrix
train_data$Heart.Disease <- as.numeric(train_data$Heart.Disease)
corMatMy <- cor(train_data[, 1:ncol(train_data)-1])
corrplot(corMatMy, order = "hclust")

#Apply correlation filter at 0.70 (absolute correlation cutoff)
highlyCor <- colnames(train_data[,1:ncol(train_data)-1])[findCorrelation(corMatMy, cutoff = 0.7, verbose = TRUE)]
```

```
## All correlations <= 0.7
```

```r
## Compare row 2  and column  3 with corr  0.899 
##   Means:  0.696 vs 0.575 so flagging column 2 
## Compare row 3  and column  7 with corr  0.736 
##   Means:  0.654 vs 0.55 so flagging column 3 
## All correlations <= 0.7
# which variables are flagged for removal?
highlyCor
```

```
## character(0)
```

```r
## [1] "uniformity_of_cell_size"  "uniformity_of_cell_shape"
#then we remove these variables
train_data_cor <- train_data[, which(!colnames(train_data) %in% highlyCor)]
```

<img src="figure/manual-unnamed-chunk-13-1.png" style="display: block; margin: auto;" />
  
In order to get first look at classification process, we plot classification decision trees.  As we can see from this plot, the features "chol" and "thalach" are the most dominant among all the other attributs.

#### EDA Conclusion  
In conclusion from the following graphs we can see that there are mostly people with level 1 hear disease. In addition we see that there are more males that participated in this data than females. Also there are more males that have a strong heart disease (level 4).  
We can see the the heart disease is a factor of age as most of the strongest heart disease (level 4) accure between the ages of 30 to 70. Cholesterol is also a big factor for causing heart diseases and so from the graph we see that less than 275, you will almost definetly have a heart disease. Lastly from the correlation graph we see that the biggest factor on heart diseases is the Cholesterol and so Cholesterol has the biggest effect on heart diseases according to the tested parameters.  

#### CLASSIFICATION  
  
To get first look at the classification process, we plot a schematic classification decision tree.  


```r
#####CLASSIFICATION######
train_data$Heart.Disease <- as.factor(train_data$Heart.Disease)
test_data$Heart.Disease <- as.factor(test_data$Heart.Disease)

set.seed(42)
fit <- rpart(Heart.Disease ~ .,
            data = train_data,
            method = "class",
            control = rpart.control(xval = 10, 
                                    minbucket = 2, 
                                    cp = 0), 
             parms = list(split = "information"))

rpart.plot(fit, extra = 100)
```

<img src="figure/manual-unnamed-chunk-14-1.png" style="display: block; margin: auto;" />
As we can see from this plot, the features "chol" and "thalach" are the most dominant among all the other attributs.

  Chol - represent the serum cholestoral amount of the patient.
  Thalach - represent the maximum heart rate achived of the patient.
  
It make sense to see these findings because for the cholesterol, when there is too much cholesterol in your blood, it builds up in the walls of your arteries, causing a process called atherosclerosis, a form of heart disease.  

And for the thalach, several large observational studies have indicated that there is correlation.

This decision tree leads us to explore the prediction accuracy level to claasify the heart disease to 5 levels (0 - healthy, 1-4 hear disease levels).

### Predection Models  

#### Knn Model  

First, we understanded that in order to answer out research question, we need to use classification models.  

This is the first model we wanted to apply on our data.  

The Knn model can be used for both classification and regression predictive problems. However it is more likely to use it for classificationץ.  

We chose to use this model because it is ease to interpret the output and it take short calculation time.  



```r
Accuracy_List <- list()

set.seed(42)
knn_fit <- train(Heart.Disease~., 
                 data = train_data, 
                 method = "knn",
                 trControl=trainControl(method = "repeatedcv", 
                                        number = 10, 
                                        repeats = 3),
                 preProcess = c("center", "scale"),
                 tuneLength = 10)

confusion_mat <- confusionMatrix(predict(knn_fit, test_data), test_data$Heart.Disease)
confusion_mat
```

```
## Confusion Matrix and Statistics
## 
##           Reference
## Prediction  0  1  2  3  4
##          0 12  8  1  1  0
##          1 11 19  3  0  9
##          2  0  2  7  0  1
##          3  0  0  0  0  0
##          4  0  4  3  0  9
## 
## Overall Statistics
##                                           
##                Accuracy : 0.5222          
##                  95% CI : (0.4143, 0.6287)
##     No Information Rate : 0.3667          
##     P-Value [Acc > NIR] : 0.001857        
##                                           
##                   Kappa : 0.3286          
##                                           
##  Mcnemar's Test P-Value : NA              
## 
## Statistics by Class:
## 
##                      Class: 0 Class: 1 Class: 2 Class: 3 Class: 4
## Sensitivity            0.5217   0.5758  0.50000  0.00000   0.4737
## Specificity            0.8507   0.5965  0.96053  1.00000   0.9014
## Pos Pred Value         0.5455   0.4524  0.70000      NaN   0.5625
## Neg Pred Value         0.8382   0.7083  0.91250  0.98889   0.8649
## Prevalence             0.2556   0.3667  0.15556  0.01111   0.2111
## Detection Rate         0.1333   0.2111  0.07778  0.00000   0.1000
## Detection Prevalence   0.2444   0.4667  0.11111  0.00000   0.1778
## Balanced Accuracy      0.6862   0.5861  0.73026  0.50000   0.6875
```

```r
Accuracy_List$knn <- confusion_mat$overall['Accuracy']
```

As we can see, the results of this model are not so good becase the acuuracy level is very  close to random classification ~ 0.5222.  

In this case, we decided to search for a better model instead of trying to improve the current one.

Therefor, we wanted to try the decision trees models because of the initial result from the schematic decision tree above.

  
#### Core Parralel For Multi Processing  

The next model suppose to take much longer time to run, so we used core parralel configaration in order to short the procces time. We faced this option while exploring the implemantations of the XGB model (which without it, it takes forever). The principle of this method is to check the number of the cores of the computer and then run a function that use all of them to run the code.



```r
 # configure multicore
 cl <- makeCluster(detectCores())
 registerDoParallel(cl)
```

#### Random Forest model  

Random Forest (RF) is model from the family of decision trees.  
In order to evaluate the model, we look at the confusion matrix and plot the prediction results for each class (Heart disease level). 

Random forest, like its name implies, consists of a large number of individual decision trees that operate as an ensemble. Each individual tree in the random forest spits out a class prediction and the class with the most votes becomes our model's prediction.  
The fundamental concept behind random forest is a simple but powerful one - the wisdom of crowds. In data science speak, the reason that the random forest model works so well is:
A large number of relatively uncorrelated models (trees) operating as a committee will outperform any of the individual constituent models.  
The low correlation between models is the key. Just like how investments with low correlations (like stocks and bonds) come together to form a portfolio that is greater than the sum of its parts, uncorrelated models can produce ensemble predictions that are more accurate than any of the individual predictions. The reason for this wonderful effect is that the trees protect each other from their individual errors (as long as they don't constantly all err in the same direction). While some trees may be wrong, many other trees will be right, so as a group the trees are able to move in the correct direction.  
In order to evaluate the model, we look at the confusion matrix and plot the prediction results for each class (Heart disease level). 


#### Results:

##### Evaluation for the training set


```r
### RANDOM FORREST ###
set.seed(42)
model_rf <- caret::train(Heart.Disease ~ .,
                         data = train_data,
                         method = "rf",
                         metric = "Accuracy",
                         importance = T,
                         preProcess = c("scale", "center"),
                         trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  savePredictions = TRUE, 
                                                  verboseIter = FALSE))

# confusion matrix
model_rf
```

```
## Random Forest 
## 
## 213 samples
##  13 predictor
##   5 classes: '0', '1', '2', '3', '4' 
## 
## Pre-processing: scaled (13), centered (13) 
## Resampling: Cross-Validated (10 fold, repeated 3 times) 
## Summary of sample sizes: 193, 192, 192, 191, 192, 191, ... 
## Resampling results across tuning parameters:
## 
##   mtry  Accuracy   Kappa    
##    2    0.9065948  0.8719455
##    7    0.9546935  0.9385615
##   13    0.9514396  0.9342690
## 
## Accuracy was used to select the optimal model using the largest value.
## The final value used for the model was mtry = 7.
```

```r
model_rf$finalModel$confusion 
```

```
##    0  1  2 3  4 class.error
## 0 58  2  0 0  0  0.03333333
## 1  1 68  2 0  1  0.05555556
## 2  0  1 33 0  0  0.02941176
## 3  1  0  0 3  0  0.25000000
## 4  0  2  0 0 41  0.04651163
```

##### Let's see the feature importance for each heart disease level:


```r
# estimate variable importance per descision level
importance <- varImp(model_rf, scale = TRUE)
plot(importance)
```

<img src="figure/manual-unnamed-chunk-18-1.png" style="display: block; margin: auto;" />

We can see that as we saw in the schematic decision tree, the most important features that have imfluance on the heart disease level decision are chol and thalach.

##### Evaluation for the test set


```r
# Predicting test data
confusion_mat <- confusionMatrix(predict(model_rf, test_data), as.factor(test_data$Heart.Disease))
confusion_mat
```

```
## Confusion Matrix and Statistics
## 
##           Reference
## Prediction  0  1  2  3  4
##          0 23  1  0  0  0
##          1  0 31  0  0  0
##          2  0  1 14  0  0
##          3  0  0  0  1  0
##          4  0  0  0  0 19
## 
## Overall Statistics
##                                          
##                Accuracy : 0.9778         
##                  95% CI : (0.922, 0.9973)
##     No Information Rate : 0.3667         
##     P-Value [Acc > NIR] : < 2.2e-16      
##                                          
##                   Kappa : 0.9698         
##                                          
##  Mcnemar's Test P-Value : NA             
## 
## Statistics by Class:
## 
##                      Class: 0 Class: 1 Class: 2 Class: 3 Class: 4
## Sensitivity            1.0000   0.9394   1.0000  1.00000   1.0000
## Specificity            0.9851   1.0000   0.9868  1.00000   1.0000
## Pos Pred Value         0.9583   1.0000   0.9333  1.00000   1.0000
## Neg Pred Value         1.0000   0.9661   1.0000  1.00000   1.0000
## Prevalence             0.2556   0.3667   0.1556  0.01111   0.2111
## Detection Rate         0.2556   0.3444   0.1556  0.01111   0.2111
## Detection Prevalence   0.2667   0.3444   0.1667  0.01111   0.2111
## Balanced Accuracy      0.9925   0.9697   0.9934  1.00000   1.0000
```

Confusion matrix is a specific table layout that allows visualization of the performance of an algorithm.  
Each row of the matrix represents the instances in a predicted class while each column represents the instances in an actual class.  

As shown in our matrix, the diagonal include values which are very close to the maximum. We also see that the accuracy level is very high ~ 0.9778 and the sensitivity and specificity levels are close to 100% that is very important in our classification case because we are dealing with the health care topic.


```r
Accuracy_List$rf <- confusion_mat$overall['Accuracy']

# Plot the results
results <- data.frame(actual = as.factor(test_data$Heart.Disease),
                      predict(model_rf, test_data, type = "prob"))

results$prediction <- ifelse(results$X0 > 0.25, "0",
                             ifelse(results$X1 > 0.25, "1", 
                                    ifelse(results$X2 > 0.25, "2", 
                                           ifelse(results$X3 > 0.25, "3",
                                                  ifelse(results$X4 > 0.25, "4", NA)))))

results$correct <- ifelse(results$actual == as.factor(results$prediction), TRUE, FALSE)

ggplot(results, aes(x = prediction, fill = correct)) +
  geom_bar(position = "dodge")
```

<img src="figure/manual-unnamed-chunk-20-1.png" style="display: block; margin: auto;" />

Plot of the predictions results.  


```r
results_gather <- gather(results,key=f,value=v,c("X0","X1","X2","X3","X4"))
ggplot(results_gather, aes(x =prediction , y = v, color = correct, shape = correct)) +
  geom_jitter(size = 3, alpha = 0.6)+facet_wrap( ~ f, scales = "free", ncol = 3)
```

<img src="figure/manual-unnamed-chunk-21-1.png" style="display: block; margin: auto;" />

This graph presents the prediction numeric value for each heart disease level(X1,X2,X3,X4).  

y axis represents the prediction frequencies values and x axis represents the prediction parameters (e.g Lets look at X0 graph: triangle at "0" x axis column with 0.75 y value means that 0 is recognized as 0 with 75% assurance level). 
shape and color represent TRUE/FALSE prediction.  

#### Stochastic Gradient Boosting Model 

The next model is GBM which suppose to be an improve model of random forest.

Gradient boosting is a machine learning technique for regression and classification problems, which produces a prediction model in the form of an ensemble of weak prediction models, typically decision trees. It builds the model in a stage-wise fashion like other boosting methods do, and it generalizes them by allowing optimization of an arbitrary differentiable loss function.  
The idea of gradient boosting originated in the observation by Leo Breiman that boosting can be interpreted as an optimization algorithm on a suitable cost function. Explicit regression gradient boosting algorithms were subsequently developed by Jerome H. Friedman, simultaneously with the more general functional gradient boosting perspective of Llew Mason, Jonathan Baxter, Peter Bartlett and Marcus Frean. The latter two papers introduced the view of boosting algorithms as iterative functional gradient descent algorithms. That is, algorithms that optimize a cost function over function space by iteratively choosing a function (weak hypothesis) that points in the negative gradient direction. This functional gradient view of boosting has led to the development of boosting algorithms in many areas of machine learning and statistics beyond regression and classification.  


```r
set.seed(42)
model_gbm <- caret::train(Heart.Disease ~ .,
                          data = train_data,
                          method = "gbm",
                          trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  verboseIter = FALSE),
                          verbose = 0)
model_gbm
```

```
## Stochastic Gradient Boosting 
## 
## 213 samples
##  13 predictor
##   5 classes: '0', '1', '2', '3', '4' 
## 
## No pre-processing
## Resampling: Cross-Validated (10 fold, repeated 3 times) 
## Summary of sample sizes: 193, 192, 192, 191, 192, 191, ... 
## Resampling results across tuning parameters:
## 
##   interaction.depth  n.trees  Accuracy   Kappa    
##   1                   50      0.9387195  0.9161919
##   1                  100      0.9517291  0.9344339
##   1                  150      0.9535401  0.9369759
##   2                   50      0.9454583  0.9254439
##   2                  100      0.9486329  0.9299231
##   2                  150      0.9455305  0.9257618
##   3                   50      0.9453068  0.9252849
##   3                  100      0.9487123  0.9300027
##   3                  150      0.9471250  0.9278463
## 
## Tuning parameter 'shrinkage' was held constant at a value of 0.1
## 
## Tuning parameter 'n.minobsinnode' was held constant at a value of 10
## Accuracy was used to select the optimal model using the largest value.
## The final values used for the model were n.trees = 150,
##  interaction.depth = 1, shrinkage = 0.1 and n.minobsinnode = 10.
```

```r
confusion_mat <- confusionMatrix(  data = predict(model_gbm, test_data), reference = as.factor(test_data$Heart.Disease))
confusion_mat
```

```
## Confusion Matrix and Statistics
## 
##           Reference
## Prediction  0  1  2  3  4
##          0 22  1  0  0  0
##          1  0 31  0  0  0
##          2  0  1 13  0  0
##          3  1  0  0  1  0
##          4  0  0  1  0 19
## 
## Overall Statistics
##                                           
##                Accuracy : 0.9556          
##                  95% CI : (0.8901, 0.9878)
##     No Information Rate : 0.3667          
##     P-Value [Acc > NIR] : < 2.2e-16       
##                                           
##                   Kappa : 0.9397          
##                                           
##  Mcnemar's Test P-Value : NA              
## 
## Statistics by Class:
## 
##                      Class: 0 Class: 1 Class: 2 Class: 3 Class: 4
## Sensitivity            0.9565   0.9394   0.9286  1.00000   1.0000
## Specificity            0.9851   1.0000   0.9868  0.98876   0.9859
## Pos Pred Value         0.9565   1.0000   0.9286  0.50000   0.9500
## Neg Pred Value         0.9851   0.9661   0.9868  1.00000   1.0000
## Prevalence             0.2556   0.3667   0.1556  0.01111   0.2111
## Detection Rate         0.2444   0.3444   0.1444  0.01111   0.2111
## Detection Prevalence   0.2556   0.3444   0.1556  0.02222   0.2222
## Balanced Accuracy      0.9708   0.9697   0.9577  0.99438   0.9930
```

```r
Accuracy_List$gbm<- confusion_mat$overall['Accuracy']
```

Unfortunaitely, the results of this model wan not better than random forest model. 

#### XGB Model 

After the last model we chose to implement the XGB model which suppose to be an improve model of the GBM (as we read on the internet).

XGBoost is one of the implementations of Gradient Boosting concept, but what makes XGBoost unique is that it uses a more regularized model formalization to control over-fitting, which gives it better performance. Therefore, it helps to reduce overfitting.  
And as you would expect, there is an R package called 'xgboost', which is an interface to the XGBoost library. And the amazing thing about this is that not only it produces much better prediction performance compared to other algorithms in general, but also it completes the tasks with blazing fast speed. It automatically does parallel computation on a single machine which could be more than 10 times faster than existing gradient boosting packages.  


```r
# XGB Model 
set.seed(42)
train_data$Heart.Disease <- as.factor(train_data$Heart.Disease)
model_xgb <- caret::train(Heart.Disease ~ .,
                          data = train_data,
                          method = "xgbTree",
                          preProcess = c("scale", "center"),
                          trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  savePredictions = TRUE, 
                                                  verboseIter = FALSE))

# Feature importance 
importance <- varImp(model_xgb, scale = TRUE)
plot(importance)

# predicting test data
# confusion matrix
confusion_mat <- confusionMatrix(predict(model_xgb, test_data), as.factor(test_data$Heart.Disease))
confusion_mat
```

```
## Confusion Matrix and Statistics
## 
##           Reference
## Prediction  0  1  2  3  4
##          0 23  0  0  0  0
##          1  0 31  0  0  0
##          2  0  2 13  0  0
##          3  0  0  0  1  0
##          4  0  0  1  0 19
## 
## Overall Statistics
##                                           
##                Accuracy : 0.9667          
##                  95% CI : (0.9057, 0.9931)
##     No Information Rate : 0.3667          
##     P-Value [Acc > NIR] : < 2.2e-16       
##                                           
##                   Kappa : 0.9547          
##                                           
##  Mcnemar's Test P-Value : NA              
## 
## Statistics by Class:
## 
##                      Class: 0 Class: 1 Class: 2 Class: 3 Class: 4
## Sensitivity            1.0000   0.9394   0.9286  1.00000   1.0000
## Specificity            1.0000   1.0000   0.9737  1.00000   0.9859
## Pos Pred Value         1.0000   1.0000   0.8667  1.00000   0.9500
## Neg Pred Value         1.0000   0.9661   0.9867  1.00000   1.0000
## Prevalence             0.2556   0.3667   0.1556  0.01111   0.2111
## Detection Rate         0.2556   0.3444   0.1444  0.01111   0.2111
## Detection Prevalence   0.2556   0.3444   0.1667  0.01111   0.2222
## Balanced Accuracy      1.0000   0.9697   0.9511  1.00000   0.9930
```

```r
Accuracy_List$xgb <- confusion_mat$overall['Accuracy']

# Plo true and false results
results <- data.frame(actual = as.factor(test_data$Heart.Disease),
                      predict(model_xgb, test_data, type = "prob"))

results$prediction <- ifelse(results$X0 > 0.25, "0",
                             ifelse(results$X1 > 0.25, "1", 
                                    ifelse(results$X2 > 0.25, "2", 
                                           ifelse(results$X3 > 0.25, "3",
                                                  ifelse(results$X4 > 0.25, "4", NA)))))

results$correct <- ifelse(results$actual == as.factor(results$prediction), TRUE, FALSE)

ggplot(results, aes(x = prediction, fill = correct)) +
  geom_bar(position = "dodge")
```

<img src="figure/manual-unnamed-chunk-23-1.png" style="display: block; margin: auto;" /><img src="figure/manual-unnamed-chunk-23-2.png" style="display: block; margin: auto;" />

Conclusion of XGB model results:  

The accuracy level is 0.9667 which is better than the GBM model as we expected. However, it is still lower than the accuracy of random forest model.
As regarding to the importance features, we see the same results as the other models.
We can note that it can be seen on the graph that this model have prediction for heart disease level 1. (On the XGB model there is no false prediction for level 1 while in random forest we do have some falses predictions)


```r
results_gather <- gather(results,key=f,value=v,c("X0","X1","X2","X3","X4"))
ggplot(results_gather, aes(x =prediction , y = v, color = correct, shape = correct)) +
  geom_jitter(size = 3, alpha = 0.6)+facet_wrap( ~ f, scales = "free", ncol = 3)
```

<img src="figure/manual-unnamed-chunk-24-1.png" style="display: block; margin: auto;" />

This graph presents the prediction numeric value for each heart disease level(X1,X2,X3,X4).  

y axis represents the prediction frequencies values and x axis represents the prediction parameters (e.g Lets look at X0 graph: triangle at "0" x axis column with 0.75 y value means that 0 is recognized as 0 with 75% assurance level) . shape and color represent TRUE/FALSE prediction.  


# Choosing the best model (by accuracy level).  

Showing a comparison table for the above models.
We chose to look on the accuracy levels only because all the other parameters were very close to each other through the models.


```r
(as.data.frame(Accuracy_List))
```

```
##                knn        rf       gbm       xgb
## Accuracy 0.5222222 0.9777778 0.9555556 0.9666667
```

As we can see in the table above, the model *Random Forest* got the best acuuracy.
In this case we will try to improve its accuracy in the following process.

#### Recursive Feature Elimination (RFE)  

RFE uses a Random Forest algorithm to test combinations of features and rate each with an accuracy score. The combination with the highest score is usually preferential.  
As we can see from the results of the RFE process, as we expected from the classification tree from the begining, the most important features are *Cholesterol* and *Thalach*.
Now, we will first try to improve our accuracy by tunning the Random Forest model and then we will try to run the model again while using smaller dataset that contain the most important features only (as a comparisson to the Heart.Disease column).

```r
set.seed(42)
results_rfe <- rfe(x = train_data[, 1:ncol(train_data)-1], 
                   y = train_data$Heart.Disease, 
                   sizes = c(1:9), 
                   metric = "Accuracy",
                   rfeControl = rfeControl(functions = rfFuncs, method = "cv", number = 10))
# chosen features with the most importance values
predictors(results_rfe)
```

```
## [1] "chol"    "thalach"
```

```r
# save the train data after rfe feature selevtion algorithm
train_data_rfe <- train_data[, c(1, which(colnames(train_data) %in% predictors(results_rfe)))]
```

# Accuracy results after implement the model improvement

```r
### RANDOM FORREST ###
set.seed(42)
train_data$Heart.Disease <- as.factor(train_data$Heart.Disease)
train_data_improve <- train_data[,c("chol","thalach","Heart.Disease")]
model_rf <- caret::train(Heart.Disease ~ .,
                         data = train_data_improve,
                         method = "rf",
                         metric = "Accuracy",
                         importance = T,
                         preProcess = c("scale", "center"),
                         trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  savePredictions = TRUE, 
                                                  verboseIter = FALSE))
```

```
## note: only 1 unique complexity parameters in default grid. Truncating the grid to 1 .
```

```r
# Predicting test data
test_data$Heart.Disease <- as.factor(test_data$Heart.Disease)
test_data_improve <- test_data[,c("chol","thalach","Heart.Disease")]
confusionMatrix(predict(model_rf, test_data_improve), test_data_improve$Heart.Disease)
```

```
## Confusion Matrix and Statistics
## 
##           Reference
## Prediction  0  1  2  3  4
##          0 23  1  0  0  0
##          1  0 31  0  0  0
##          2  0  1 14  0  0
##          3  0  0  0  1  0
##          4  0  0  0  0 19
## 
## Overall Statistics
##                                          
##                Accuracy : 0.9778         
##                  95% CI : (0.922, 0.9973)
##     No Information Rate : 0.3667         
##     P-Value [Acc > NIR] : < 2.2e-16      
##                                          
##                   Kappa : 0.9698         
##                                          
##  Mcnemar's Test P-Value : NA             
## 
## Statistics by Class:
## 
##                      Class: 0 Class: 1 Class: 2 Class: 3 Class: 4
## Sensitivity            1.0000   0.9394   1.0000  1.00000   1.0000
## Specificity            0.9851   1.0000   0.9868  1.00000   1.0000
## Pos Pred Value         0.9583   1.0000   0.9333  1.00000   1.0000
## Neg Pred Value         1.0000   0.9661   1.0000  1.00000   1.0000
## Prevalence             0.2556   0.3667   0.1556  0.01111   0.2111
## Detection Rate         0.2556   0.3444   0.1556  0.01111   0.2111
## Detection Prevalence   0.2667   0.3444   0.1667  0.01111   0.2111
## Balanced Accuracy      0.9925   0.9697   0.9934  1.00000   1.0000
```
We got the same accuracy level as we already achieved, there is no improvement. 
It's seems like we actually achieve the top level of accuracy in random forest model. 

#### Grid search with caret - Automatic Grid 
##### Searching for the best mtry parameter
Now, lets try another way to improve the rf model. Here, we searching for the best model parameteres for RF. 

```r
set.seed(42)
model_rf_tune_auto <- caret::train(Heart.Disease ~ .,
                         data = train_data,
                         method = "rf",
                         preProcess = c("scale", "center"),
                         trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  savePredictions = TRUE, 
                                                  verboseIter = FALSE,
                                                  search = "random"),
                         tuneLength = 15)
model_rf_tune_auto
```

```
## Random Forest 
## 
## 213 samples
##  13 predictor
##   5 classes: '0', '1', '2', '3', '4' 
## 
## Pre-processing: scaled (13), centered (13) 
## Resampling: Cross-Validated (10 fold, repeated 3 times) 
## Summary of sample sizes: 193, 192, 192, 191, 192, 191, ... 
## Resampling results across tuning parameters:
## 
##   mtry  Accuracy   Kappa    
##    2    0.8926699  0.8527010
##    4    0.9221855  0.8939469
##    6    0.9455242  0.9257440
##    7    0.9530269  0.9362501
##    9    0.9529547  0.9362956
##   10    0.9514396  0.9342357
##   11    0.9529547  0.9363936
##   13    0.9498522  0.9321669
## 
## Accuracy was used to select the optimal model using the largest value.
## The final value used for the model was mtry = 7.
```

```r
plot(model_rf_tune_auto)

set.seed(42)
rf_gridsearch <- train(Heart.Disease ~ ., 
                       data = train_data,
                       method = "rf",
                       metric = "Accuracy",
                       tuneGrid = expand.grid(.mtry = c(1:15)),
                       trControl = trainControl(method = "repeatedcv",
                                                number = 10,
                                                repeats = 3,
                                                search = "grid"))
rf_gridsearch
```

```
## Random Forest 
## 
## 213 samples
##  13 predictor
##   5 classes: '0', '1', '2', '3', '4' 
## 
## No pre-processing
## Resampling: Cross-Validated (10 fold, repeated 3 times) 
## Summary of sample sizes: 193, 192, 192, 191, 192, 191, ... 
## Resampling results across tuning parameters:
## 
##   mtry  Accuracy   Kappa    
##    1    0.7062921  0.5818457
##    2    0.9000797  0.8625890
##    3    0.9287007  0.9027563
##    4    0.9266588  0.8999844
##    5    0.9316516  0.9068649
##    6    0.9469600  0.9278749
##    7    0.9502139  0.9322786
##    8    0.9560572  0.9404612
##    9    0.9562087  0.9406466
##   10    0.9562087  0.9406466
##   11    0.9531062  0.9364789
##   12    0.9530269  0.9364746
##   13    0.9514396  0.9342690
##   14    0.9529547  0.9363346
##   15    0.9530269  0.9364162
## 
## Accuracy was used to select the optimal model using the largest value.
## The final value used for the model was mtry = 9.
```

```r
plot(rf_gridsearch)


# Random Search
set.seed(42)
mtry <- sqrt(ncol(train_data))
rf_random <- train(Heart.Disease ~ .,
                   data = train_data,
                   method = "rf",
                   metric = "Accuracy",
                   tuneLength = 15,
                   trControl = trainControl(method = "repeatedcv",
                                            number = 10,
                                            repeats = 3,
                                            search = "random"))
rf_random
```

```
## Random Forest 
## 
## 213 samples
##  13 predictor
##   5 classes: '0', '1', '2', '3', '4' 
## 
## No pre-processing
## Resampling: Cross-Validated (10 fold, repeated 3 times) 
## Summary of sample sizes: 193, 192, 192, 191, 192, 191, ... 
## Resampling results across tuning parameters:
## 
##   mtry  Accuracy   Kappa    
##    2    0.9048560  0.8698770
##    4    0.9252952  0.8982184
##    6    0.9487060  0.9299551
##    7    0.9471115  0.9281188
##    9    0.9562808  0.9406810
##   10    0.9529547  0.9362956
##   11    0.9529547  0.9363007
##   13    0.9498522  0.9321669
## 
## Accuracy was used to select the optimal model using the largest value.
## The final value used for the model was mtry = 9.
```

```r
plot(rf_random)
```

<img src="figure/manual-unnamed-chunk-28-1.png" style="display: block; margin: auto;" /><img src="figure/manual-unnamed-chunk-28-2.png" style="display: block; margin: auto;" /><img src="figure/manual-unnamed-chunk-28-3.png" style="display: block; margin: auto;" />
In our case mtry=10 is the best parameter to achieve the best accuracy.  


##### Searching for the best ntree parameter
We know that decision trees family models are very sensitive to the number of trees. Let's check which is the best for our model.

```r
modellist <- list()
for (ntree in c(1, 5, 10, 100, 1000)) {
    set.seed(42)
    fit <- train(Heart.Disease ~ .,
                 data = train_data,
                 method = "rf",
                 metric = "Accuracy",
                 tuneGrid = expand.grid(.mtry = 9),
                 trControl = trainControl(method = "repeatedcv",
                                          number = 10, 
                                          repeats = 3,
                                          search = "grid"),
                 ntree = ntree)
    key <- toString(ntree)
    modellist[[key]] <- fit
}
# compare results
results <- resamples(modellist)
results_summary <- summary(results)
results_summary
```

```
## 
## Call:
## summary.resamples(object = results)
## 
## Models: 1, 5, 10, 100, 1000 
## Number of resamples: 30 
## 
## Accuracy 
##           Min.   1st Qu.    Median      Mean   3rd Qu. Max. NA's
## 1    0.7142857 0.8695652 0.9295455 0.9075165 0.9540043    1    0
## 5    0.8095238 0.9100791 0.9772727 0.9559056 1.0000000    1    0
## 10   0.9000000 0.9130435 0.9545455 0.9577960 1.0000000    1    0
## 100  0.8500000 0.9505952 0.9545455 0.9562087 1.0000000    1    0
## 1000 0.8095238 0.9500000 0.9545455 0.9529547 1.0000000    1    0
## 
## Kappa 
##           Min.   1st Qu.    Median      Mean   3rd Qu. Max. NA's
## 1    0.6315789 0.8235810 0.9054524 0.8747892 0.9373003    1    0
## 5    0.7399381 0.8783854 0.9698630 0.9401307 1.0000000    1    0
## 10   0.8611111 0.8816203 0.9382022 0.9429237 1.0000000    1    0
## 100  0.7938144 0.9316906 0.9382022 0.9406688 1.0000000    1    0
## 1000 0.7407407 0.9310901 0.9382022 0.9363013 1.0000000    1    0
```

```r
plot(x = as.factor(c(1, 5, 10, 100, 1000)), results_summary$statistics$Accuracy[,"Mean"], xlab = "ntree", ylab = "accuracy mean (over 30 iterations)", type = "c")
```

<img src="figure/manual-unnamed-chunk-29-1.png" style="display: block; margin: auto;" />
As we can see from the model tuning, the best number of trees for our model, aside to the value of mtry = 9 (that we have found before) is ntrees = 10. The *mean* accuracy value of ntrees=10 over 30 iterations is 0.9577960 which is the highest accuracy mean compare to n=1/5/100/1000.  

## Implementation of model improvement parameters
#### Accuracy results after implement the model improvement (Random Forest)
Here we run RF model with mtry=9 and ntree=10.

```r
### Random Forest ###
set.seed(42)
model_rf <- caret::train(Heart.Disease ~ .,
                         data = train_data,
                         method = "rf",
                         metric = "Accuracy",
                         importance = T,
                         ntree = 10,
                         tuneGrid = expand.grid(mtry = 9),
                         preProcess = c("scale", "center"),
                         trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  savePredictions = TRUE, 
                                                  verboseIter = FALSE))

# confusion matrix
model_rf
```

```
## Random Forest 
## 
## 213 samples
##  13 predictor
##   5 classes: '0', '1', '2', '3', '4' 
## 
## Pre-processing: scaled (13), centered (13) 
## Resampling: Cross-Validated (10 fold, repeated 3 times) 
## Summary of sample sizes: 193, 192, 192, 191, 192, 191, ... 
## Resampling results:
## 
##   Accuracy   Kappa    
##   0.9481134  0.9295046
## 
## Tuning parameter 'mtry' was held constant at a value of 9
```

```r
# Predicting test data
test_data_improve <- test_data
confusionMatrix(predict(model_rf, test_data_improve), test_data_improve$Heart.Disease)
```

```
## Confusion Matrix and Statistics
## 
##           Reference
## Prediction  0  1  2  3  4
##          0 23  1  0  0  0
##          1  0 31  1  0  0
##          2  0  1 13  0  0
##          3  0  0  0  1  0
##          4  0  0  0  0 19
## 
## Overall Statistics
##                                           
##                Accuracy : 0.9667          
##                  95% CI : (0.9057, 0.9931)
##     No Information Rate : 0.3667          
##     P-Value [Acc > NIR] : < 2.2e-16       
##                                           
##                   Kappa : 0.9545          
##                                           
##  Mcnemar's Test P-Value : NA              
## 
## Statistics by Class:
## 
##                      Class: 0 Class: 1 Class: 2 Class: 3 Class: 4
## Sensitivity            1.0000   0.9394   0.9286  1.00000   1.0000
## Specificity            0.9851   0.9825   0.9868  1.00000   1.0000
## Pos Pred Value         0.9583   0.9688   0.9286  1.00000   1.0000
## Neg Pred Value         1.0000   0.9655   0.9868  1.00000   1.0000
## Prevalence             0.2556   0.3667   0.1556  0.01111   0.2111
## Detection Rate         0.2556   0.3444   0.1444  0.01111   0.2111
## Detection Prevalence   0.2667   0.3556   0.1556  0.01111   0.2111
## Balanced Accuracy      0.9925   0.9609   0.9577  1.00000   1.0000
```

```r
# Plot the results
results <- data.frame(actual = as.factor(test_data_improve$Heart.Disease),
                      predict(model_rf, test_data_improve, type = "prob"))

results$prediction <- ifelse(results$X0 > 0.25, "0",
                             ifelse(results$X1 > 0.25, "1", 
                                    ifelse(results$X2 > 0.25, "2", 
                                           ifelse(results$X3 > 0.25, "3",
                                                  ifelse(results$X4 > 0.25, "4", NA)))))

results$correct <- ifelse(results$actual == as.factor(results$prediction), TRUE, FALSE)

ggplot(results, aes(x = prediction, fill = correct)) +
  geom_bar(position = "dodge")
results_gather <- gather(results,key=f,value=v,c("X0","X1","X2","X3","X4"))
ggplot(results_gather, aes(x =prediction , y = v, color = correct, shape = correct)) +
  geom_jitter(size = 3, alpha = 0.6)+facet_wrap( ~ f, scales = "free", ncol = 3)
```

<img src="figure/manual-unnamed-chunk-30-1.png" style="display: block; margin: auto;" /><img src="figure/manual-unnamed-chunk-30-2.png" style="display: block; margin: auto;" />

Although, after expecting and hoping for a minor improvement we dissapointed to find out that our accuracy level got worse.

# Summary and Conclutions

In this project we studdied that there is a significant connection between thalach and cholesterol to the classification of heart disease levels. We had a comparision between 4 different models. We noticed that models from the decision trees family were much more adequate to our problem than nearest neighbors classifications. KNN model gave us 52% accuracy, GBM gave us 95%, XGB 96% and the winner was RF model with not less than 97%! (Hell Yeah!) Although we had badass results, we wanted to try and improve them. However, we did not succeed. Maybe the rest 3% accuracy is depend on another physiologic parameter that we do not have in our data. Hopfully this project will provide some tools to the doctors' community for more accurate detection tool for heart disease and in particular for the level of the disease. 
Future work resulting from this project should include a deeper understanding of the "maximum heart rate" and "cholesterol" features. For example, finding the quantitative limit that would accurately cast on the classification of the disease level, along with an exploration of the resulting biological aspect.

#### Trying all these model for classification to Healthy ("0") or Disease ("1","2","3","4")

We were very curious about what will happen if we will try to classify only for healthy and sick patients.


```r
# change heart disease column to binary level 0/1 
dataset$Heart.Disease <- replace(dataset$Heart.Disease, dataset$Heart.Disease > 0, 1)

index <- createDataPartition(dataset$Heart.Disease, p = 0.7, list = FALSE)
train_data <- dataset[index, ]
test_data  <- dataset[-index, ]

train_data$Heart.Disease <- as.factor(train_data$Heart.Disease)
test_data$Heart.Disease <- as.factor(test_data$Heart.Disease)

set.seed(42)
model_rf <- caret::train(Heart.Disease ~ .,
                         data = train_data,
                         method = "rf",
                         metric = "Accuracy",
                         importance = T,
                         preProcess = c("scale", "center"),
                         trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  savePredictions = TRUE, 
                                                  verboseIter = FALSE))

# confusion matrix
model_rf
```

```
## Random Forest 
## 
## 213 samples
##  13 predictor
##   2 classes: '0', '1' 
## 
## Pre-processing: scaled (13), centered (13) 
## Resampling: Cross-Validated (10 fold, repeated 3 times) 
## Summary of sample sizes: 192, 192, 191, 192, 191, 192, ... 
## Resampling results across tuning parameters:
## 
##   mtry  Accuracy   Kappa    
##    2    0.9560534  0.8866492
##    7    0.9733694  0.9338395
##   13    0.9686003  0.9220933
## 
## Accuracy was used to select the optimal model using the largest value.
## The final value used for the model was mtry = 7.
```

```r
model_rf$finalModel$confusion 
```

```
##    0   1 class.error
## 0 55   3  0.05172414
## 1  4 151  0.02580645
```

```r
# estimate variable importance per descision level
importance <- varImp(model_rf, scale = TRUE)
plot(importance)

# Predicting test data
confusion_mat <- confusionMatrix(predict(model_rf, test_data), as.factor(test_data$Heart.Disease))
confusion_mat
```

```
## Confusion Matrix and Statistics
## 
##           Reference
## Prediction  0  1
##          0 22  0
##          1  3 65
##                                           
##                Accuracy : 0.9667          
##                  95% CI : (0.9057, 0.9931)
##     No Information Rate : 0.7222          
##     P-Value [Acc > NIR] : 1.395e-09       
##                                           
##                   Kappa : 0.9137          
##                                           
##  Mcnemar's Test P-Value : 0.2482          
##                                           
##             Sensitivity : 0.8800          
##             Specificity : 1.0000          
##          Pos Pred Value : 1.0000          
##          Neg Pred Value : 0.9559          
##              Prevalence : 0.2778          
##          Detection Rate : 0.2444          
##    Detection Prevalence : 0.2444          
##       Balanced Accuracy : 0.9400          
##                                           
##        'Positive' Class : 0               
## 
```

```r
# Plot the results
results <- data.frame(actual = as.factor(test_data$Heart.Disease),
                      predict(model_rf, test_data, type = "prob"))

results$prediction <- ifelse(results$X0 > 0.25, "0",
                             ifelse(results$X1 > 0.25, "1", 
                                    ifelse(results$X2 > 0.25, "2", 
                                           ifelse(results$X3 > 0.25, "3",
                                                  ifelse(results$X4 > 0.25, "4", NA)))))

results$correct <- ifelse(results$actual == as.factor(results$prediction), TRUE, FALSE)

ggplot(results, aes(x = prediction, fill = correct)) +
  geom_bar(position = "dodge")
```

<img src="figure/manual-unnamed-chunk-31-1.png" style="display: block; margin: auto;" /><img src="figure/manual-unnamed-chunk-31-2.png" style="display: block; margin: auto;" />

The results are very close to the classification of 4 levels. This find is very surpring because the data is skewed. The feature that become more relevant in this case is the difference between the sensitivity and the specificity witch became more difference. this means that it is a bad thing for health care because it may cause to miss diagnosis.


```r
stopCluster(cl)
```
