## ----include=FALSE, cache=FALSE,message=FALSE,error=FALSE,warning=FALSE----
# DO NOT REMOVE
# THIS IS FOR SETTING SOME PLOTTING PARAMETERS SO THAT YOUR PLOTS DON'T TAKE UP TOO MUCH SPACE
# IF YOU WOULD LIKE TO CHANGE THESE, SEE HELP FILES ON THE par() FUNCTION
# OR ASK FOR HELP
library(knitr)
## set global chunk options
opts_chunk$set(fig.path='figure/manual-', cache.path='cache/manual-', fig.align='center', fig.show='hold', par=TRUE)
## tune details of base graphics (http://yihui.name/knitr/hooks)
knit_hooks$set(par=function(before, options, envir){
if (before && options$fig.show!='none') par(mar=c(4,4,.2,.1),cex.lab=.95,cex.axis=.9,mgp=c(2,.7,0),tcl=-.3)
})
knitr::purl("bigdatabiu_final_project_Nimrod_Ran_v2.Rmd", output = "bigdatabiu_final_project_Nimrod_Ran.R")


## ----message=FALSE, warning= FALSE---------------------------------------
library(imager)
myimg <- load.image("image1.png")
plot(myimg)


## ---- error=FALSE, warning=FALSE, message=FALSE--------------------------
library(ggplot2)
library(Hmisc)
library(corrplot)
library(dplyr)
library(tidyr)
library(caret)
library(rpart)
library(rpart.plot)
library(doParallel)
library(e1071)
library(xgboost)


## ----message = FALSE, warning = FALSE------------------------------------
dataset <- read.csv("heart.csv",header = TRUE,sep=",")
dataset <- dataset[,-1] # delete "X"(index) column
#dataset$Heart.Disease <- as.factor(dataset$Heart.Disease)
str(dataset)  


## ----message = FALSE, warning = FALSE------------------------------------
head(dataset)


## ----message = FALSE, warning = FALSE------------------------------------
ggplot(dataset, aes(x = as.factor(Heart.Disease))) + geom_bar() + ggtitle("Disease level vs count (Levels 0 (healthy)  to 4)") + labs(x = "Heart Disease Level", y = "Count") + theme(plot.title = element_text(hjust = 0.5))


## ----message = FALSE, warning = FALSE------------------------------------
ggplot(dataset, aes(x = as.factor(sex))) + geom_bar() + ggtitle("Gender count (0 - Female, 1 - Male)")+ theme(plot.title = element_text(hjust = 0.5))
mosaicplot(dataset$sex ~ as.factor(dataset$Heart.Disease), main="Disease by Gender", shade=FALSE,color=blues9,
           xlab="Gender- 0=female, 1=male", ylab="Heart disease") 



## ----message = FALSE, warning = FALSE------------------------------------
ggplot(data = dataset, mapping = aes(x = age)) + 
  geom_freqpoly(mapping = aes(colour = as.factor(Heart.Disease)), binwidth = 5) + ggtitle("Heart Disease vs Age") + labs(colour = "Heart Disease Level", x = "Age", y = "Count") + theme(plot.title = element_text(hjust = 0.5))


## ----message = FALSE, warning = FALSE------------------------------------
ggplot(data = dataset, mapping = aes(x = chol)) + 
  geom_freqpoly(mapping = aes(colour = as.factor(Heart.Disease)), binwidth = 5) + ggtitle("Hreat Disease vs Cholesterol") + labs(x = "Cholesterol", y = "Count", colour = "Heart Disease Level") + theme(plot.title = element_text(hjust = 0.5))


## ----message = FALSE, warning = FALSE------------------------------------
ggplot(dataset, aes(y = chol, x = as.factor(Heart.Disease), fill = as.factor(dataset$sex))) + geom_boxplot() + ggtitle("Hreat Disease vs Cholesterol vs Sex") + labs(x = "Heart Disease Level", y = "Cholesterol", fill = "Sex") + theme(plot.title = element_text(hjust = 0.5))


## ----message = FALSE, warning = FALSE------------------------------------
#install.packages("corrplot")
test <- cor(dataset);
head(round(test,2))
corrplot(test, method="circle")


## ----message = FALSE, warning = FALSE------------------------------------
dataset2 <- dataset
# change heart disease column to binary level 0/1 
dataset2$Heart.Disease <- replace(dataset2$Heart.Disease, dataset2$Heart.Disease > 0, 1)

# Split to training and test set 0.7/0.3
set.seed(42)
index <- createDataPartition(dataset$Heart.Disease, p = 0.7, list = FALSE)
train_data <- dataset[index, ]
test_data  <- dataset[-index, ]

# Checking the splitting to train and test
rbind(data.frame(group = "train", train_data),
      data.frame(group = "test", test_data)) %>%
      gather(key = f, value = v ,age:Heart.Disease) %>%
  ggplot(aes(x = v, color = group, fill = group)) +
    geom_density(alpha = 0.3) +
    facet_wrap( ~ f, scales = "free", ncol = 3)


## ----message = FALSE, warning = FALSE------------------------------------
# calculate correlation matrix
train_data$Heart.Disease <- as.numeric(train_data$Heart.Disease)
corMatMy <- cor(train_data[, 1:ncol(train_data)-1])
corrplot(corMatMy, order = "hclust")

#Apply correlation filter at 0.70 (absolute correlation cutoff)
highlyCor <- colnames(train_data[,1:ncol(train_data)-1])[findCorrelation(corMatMy, cutoff = 0.7, verbose = TRUE)]
## Compare row 2  and column  3 with corr  0.899 
##   Means:  0.696 vs 0.575 so flagging column 2 
## Compare row 3  and column  7 with corr  0.736 
##   Means:  0.654 vs 0.55 so flagging column 3 
## All correlations <= 0.7
# which variables are flagged for removal?
highlyCor
## [1] "uniformity_of_cell_size"  "uniformity_of_cell_shape"
#then we remove these variables
train_data_cor <- train_data[, which(!colnames(train_data) %in% highlyCor)]


## ----message = FALSE, warning = FALSE------------------------------------
#####CLASSIFICATION######
train_data$Heart.Disease <- as.factor(train_data$Heart.Disease)
test_data$Heart.Disease <- as.factor(test_data$Heart.Disease)

set.seed(42)
fit <- rpart(Heart.Disease ~ .,
            data = train_data,
            method = "class",
            control = rpart.control(xval = 10, 
                                    minbucket = 2, 
                                    cp = 0), 
             parms = list(split = "information"))

rpart.plot(fit, extra = 100)




## ----message = FALSE, warning = FALSE------------------------------------
Accuracy_List <- list()

set.seed(42)
knn_fit <- train(Heart.Disease~., 
                 data = train_data, 
                 method = "knn",
                 trControl=trainControl(method = "repeatedcv", 
                                        number = 10, 
                                        repeats = 3),
                 preProcess = c("center", "scale"),
                 tuneLength = 10)

confusion_mat <- confusionMatrix(predict(knn_fit, test_data), test_data$Heart.Disease)
confusion_mat

Accuracy_List$knn <- confusion_mat$overall['Accuracy']


## ----message = FALSE, warning = FALSE------------------------------------
 # configure multicore
 cl <- makeCluster(detectCores())
 registerDoParallel(cl)



## ----message = FALSE, warning = FALSE------------------------------------
### RANDOM FORREST ###
set.seed(42)
model_rf <- caret::train(Heart.Disease ~ .,
                         data = train_data,
                         method = "rf",
                         metric = "Accuracy",
                         importance = T,
                         preProcess = c("scale", "center"),
                         trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  savePredictions = TRUE, 
                                                  verboseIter = FALSE))

# confusion matrix
model_rf
model_rf$finalModel$confusion 


## ----message = FALSE, warning = FALSE------------------------------------
# estimate variable importance per descision level
importance <- varImp(model_rf, scale = TRUE)
plot(importance)


## ----message = FALSE, warning = FALSE------------------------------------
# Predicting test data
confusion_mat <- confusionMatrix(predict(model_rf, test_data), as.factor(test_data$Heart.Disease))
confusion_mat


## ----message = FALSE, warning = FALSE------------------------------------
Accuracy_List$rf <- confusion_mat$overall['Accuracy']

# Plot the results
results <- data.frame(actual = as.factor(test_data$Heart.Disease),
                      predict(model_rf, test_data, type = "prob"))

results$prediction <- ifelse(results$X0 > 0.25, "0",
                             ifelse(results$X1 > 0.25, "1", 
                                    ifelse(results$X2 > 0.25, "2", 
                                           ifelse(results$X3 > 0.25, "3",
                                                  ifelse(results$X4 > 0.25, "4", NA)))))

results$correct <- ifelse(results$actual == as.factor(results$prediction), TRUE, FALSE)

ggplot(results, aes(x = prediction, fill = correct)) +
  geom_bar(position = "dodge")


## ----message = FALSE, warning = FALSE------------------------------------
results_gather <- gather(results,key=f,value=v,c("X0","X1","X2","X3","X4"))
ggplot(results_gather, aes(x =prediction , y = v, color = correct, shape = correct)) +
  geom_jitter(size = 3, alpha = 0.6)+facet_wrap( ~ f, scales = "free", ncol = 3)


## ----message = FALSE, warning = FALSE------------------------------------
set.seed(42)
model_gbm <- caret::train(Heart.Disease ~ .,
                          data = train_data,
                          method = "gbm",
                          trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  verboseIter = FALSE),
                          verbose = 0)
model_gbm

confusion_mat <- confusionMatrix(  data = predict(model_gbm, test_data), reference = as.factor(test_data$Heart.Disease))
confusion_mat

Accuracy_List$gbm<- confusion_mat$overall['Accuracy']



## ----message = FALSE, warning = FALSE------------------------------------
# XGB Model 
set.seed(42)
train_data$Heart.Disease <- as.factor(train_data$Heart.Disease)
model_xgb <- caret::train(Heart.Disease ~ .,
                          data = train_data,
                          method = "xgbTree",
                          preProcess = c("scale", "center"),
                          trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  savePredictions = TRUE, 
                                                  verboseIter = FALSE))

# Feature importance 
importance <- varImp(model_xgb, scale = TRUE)
plot(importance)

# predicting test data
# confusion matrix
confusion_mat <- confusionMatrix(predict(model_xgb, test_data), as.factor(test_data$Heart.Disease))
confusion_mat

Accuracy_List$xgb <- confusion_mat$overall['Accuracy']

# Plo true and false results
results <- data.frame(actual = as.factor(test_data$Heart.Disease),
                      predict(model_xgb, test_data, type = "prob"))

results$prediction <- ifelse(results$X0 > 0.25, "0",
                             ifelse(results$X1 > 0.25, "1", 
                                    ifelse(results$X2 > 0.25, "2", 
                                           ifelse(results$X3 > 0.25, "3",
                                                  ifelse(results$X4 > 0.25, "4", NA)))))

results$correct <- ifelse(results$actual == as.factor(results$prediction), TRUE, FALSE)

ggplot(results, aes(x = prediction, fill = correct)) +
  geom_bar(position = "dodge")


## ----message = FALSE, warning = FALSE------------------------------------
results_gather <- gather(results,key=f,value=v,c("X0","X1","X2","X3","X4"))
ggplot(results_gather, aes(x =prediction , y = v, color = correct, shape = correct)) +
  geom_jitter(size = 3, alpha = 0.6)+facet_wrap( ~ f, scales = "free", ncol = 3)


## ----message = FALSE, warning = FALSE------------------------------------
(as.data.frame(Accuracy_List))


## ----message = FALSE, warning = FALSE------------------------------------
set.seed(42)
results_rfe <- rfe(x = train_data[, 1:ncol(train_data)-1], 
                   y = train_data$Heart.Disease, 
                   sizes = c(1:9), 
                   metric = "Accuracy",
                   rfeControl = rfeControl(functions = rfFuncs, method = "cv", number = 10))
# chosen features with the most importance values
predictors(results_rfe)
# save the train data after rfe feature selevtion algorithm
train_data_rfe <- train_data[, c(1, which(colnames(train_data) %in% predictors(results_rfe)))]


## ----message = FALSE, warning = FALSE------------------------------------
### RANDOM FORREST ###
set.seed(42)
train_data$Heart.Disease <- as.factor(train_data$Heart.Disease)
train_data_improve <- train_data[,c("chol","thalach","Heart.Disease")]
model_rf <- caret::train(Heart.Disease ~ .,
                         data = train_data_improve,
                         method = "rf",
                         metric = "Accuracy",
                         importance = T,
                         preProcess = c("scale", "center"),
                         trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  savePredictions = TRUE, 
                                                  verboseIter = FALSE))

# Predicting test data
test_data$Heart.Disease <- as.factor(test_data$Heart.Disease)
test_data_improve <- test_data[,c("chol","thalach","Heart.Disease")]
confusionMatrix(predict(model_rf, test_data_improve), test_data_improve$Heart.Disease)


## ----message = FALSE, warning = FALSE------------------------------------
set.seed(42)
model_rf_tune_auto <- caret::train(Heart.Disease ~ .,
                         data = train_data,
                         method = "rf",
                         preProcess = c("scale", "center"),
                         trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  savePredictions = TRUE, 
                                                  verboseIter = FALSE,
                                                  search = "random"),
                         tuneLength = 15)
model_rf_tune_auto
plot(model_rf_tune_auto)

set.seed(42)
rf_gridsearch <- train(Heart.Disease ~ ., 
                       data = train_data,
                       method = "rf",
                       metric = "Accuracy",
                       tuneGrid = expand.grid(.mtry = c(1:15)),
                       trControl = trainControl(method = "repeatedcv",
                                                number = 10,
                                                repeats = 3,
                                                search = "grid"))
rf_gridsearch
plot(rf_gridsearch)


# Random Search
set.seed(42)
mtry <- sqrt(ncol(train_data))
rf_random <- train(Heart.Disease ~ .,
                   data = train_data,
                   method = "rf",
                   metric = "Accuracy",
                   tuneLength = 15,
                   trControl = trainControl(method = "repeatedcv",
                                            number = 10,
                                            repeats = 3,
                                            search = "random"))
rf_random
plot(rf_random)


## ----message = FALSE, warning = FALSE------------------------------------
modellist <- list()
for (ntree in c(1, 5, 10, 100, 1000)) {
    set.seed(42)
    fit <- train(Heart.Disease ~ .,
                 data = train_data,
                 method = "rf",
                 metric = "Accuracy",
                 tuneGrid = expand.grid(.mtry = 9),
                 trControl = trainControl(method = "repeatedcv",
                                          number = 10, 
                                          repeats = 3,
                                          search = "grid"),
                 ntree = ntree)
    key <- toString(ntree)
    modellist[[key]] <- fit
}
# compare results
results <- resamples(modellist)
results_summary <- summary(results)
results_summary
plot(x = as.factor(c(1, 5, 10, 100, 1000)), results_summary$statistics$Accuracy[,"Mean"], xlab = "ntree", ylab = "accuracy mean (over 30 iterations)", type = "c")


## ----message = FALSE, warning = FALSE------------------------------------
### Random Forest ###
set.seed(42)
model_rf <- caret::train(Heart.Disease ~ .,
                         data = train_data,
                         method = "rf",
                         metric = "Accuracy",
                         importance = T,
                         ntree = 10,
                         tuneGrid = expand.grid(mtry = 9),
                         preProcess = c("scale", "center"),
                         trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  savePredictions = TRUE, 
                                                  verboseIter = FALSE))

# confusion matrix
model_rf

# Predicting test data
test_data_improve <- test_data
confusionMatrix(predict(model_rf, test_data_improve), test_data_improve$Heart.Disease)

# Plot the results
results <- data.frame(actual = as.factor(test_data_improve$Heart.Disease),
                      predict(model_rf, test_data_improve, type = "prob"))

results$prediction <- ifelse(results$X0 > 0.25, "0",
                             ifelse(results$X1 > 0.25, "1", 
                                    ifelse(results$X2 > 0.25, "2", 
                                           ifelse(results$X3 > 0.25, "3",
                                                  ifelse(results$X4 > 0.25, "4", NA)))))

results$correct <- ifelse(results$actual == as.factor(results$prediction), TRUE, FALSE)

ggplot(results, aes(x = prediction, fill = correct)) +
  geom_bar(position = "dodge")
results_gather <- gather(results,key=f,value=v,c("X0","X1","X2","X3","X4"))
ggplot(results_gather, aes(x =prediction , y = v, color = correct, shape = correct)) +
  geom_jitter(size = 3, alpha = 0.6)+facet_wrap( ~ f, scales = "free", ncol = 3)



## ----message = FALSE, warning = FALSE------------------------------------
# change heart disease column to binary level 0/1 
dataset$Heart.Disease <- replace(dataset$Heart.Disease, dataset$Heart.Disease > 0, 1)

index <- createDataPartition(dataset$Heart.Disease, p = 0.7, list = FALSE)
train_data <- dataset[index, ]
test_data  <- dataset[-index, ]

train_data$Heart.Disease <- as.factor(train_data$Heart.Disease)
test_data$Heart.Disease <- as.factor(test_data$Heart.Disease)

set.seed(42)
model_rf <- caret::train(Heart.Disease ~ .,
                         data = train_data,
                         method = "rf",
                         metric = "Accuracy",
                         importance = T,
                         preProcess = c("scale", "center"),
                         trControl = trainControl(method = "repeatedcv", 
                                                  number = 10, 
                                                  repeats = 3, 
                                                  savePredictions = TRUE, 
                                                  verboseIter = FALSE))

# confusion matrix
model_rf
model_rf$finalModel$confusion 

# estimate variable importance per descision level
importance <- varImp(model_rf, scale = TRUE)
plot(importance)

# Predicting test data
confusion_mat <- confusionMatrix(predict(model_rf, test_data), as.factor(test_data$Heart.Disease))
confusion_mat

# Plot the results
results <- data.frame(actual = as.factor(test_data$Heart.Disease),
                      predict(model_rf, test_data, type = "prob"))

results$prediction <- ifelse(results$X0 > 0.25, "0",
                             ifelse(results$X1 > 0.25, "1", 
                                    ifelse(results$X2 > 0.25, "2", 
                                           ifelse(results$X3 > 0.25, "3",
                                                  ifelse(results$X4 > 0.25, "4", NA)))))

results$correct <- ifelse(results$actual == as.factor(results$prediction), TRUE, FALSE)

ggplot(results, aes(x = prediction, fill = correct)) +
  geom_bar(position = "dodge")



## ----message = FALSE, warning = FALSE------------------------------------

stopCluster(cl)

